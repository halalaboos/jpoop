# README #

### What is this repository for? ###

* An extensive library that interprets poo files for usage inside of the java vm
* version 1.0

### How do I get set up? ###

* No library dependencies whatsoever, entirely poo free.

### Examples ###
There is an example .poo file provided for you within the library itself.

System.out.println(net.halalaboos.jpoop.PoopEngine.runPoop(<poop code>));

That is how we run our .poo files.

We can convert lines of text into .poo format.
net.halalaboos.jpoop.PoopEngine.toPoop("Hamburger");
