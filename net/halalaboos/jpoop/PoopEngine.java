package net.halalaboos.jpoop;

public class PoopEngine {

	private static final String EAT = "eat", 
			PUKE = "puke", 
			POOP = "poop", 
			POOPU = "POOP",
			SNIFF = "sniff", 
			FLUSH = "flush";
	
	private static String alphabet = "0123456789abcdefghijklmnopqrstuvwxyz.,-!?+*<>#@$��%&/()[] ";
	
	public static String runPoop(String poop) {
		int pointer = 0;
		StringBuilder output = new StringBuilder();
		String temp = "";
		if (poop.contains("\n")) {
			String[] lines = poop.split("\n");
			for (String line : lines) {
				String[] split = line.split(" ");
				for (String input : split) {
					if (!input.isEmpty()) {
						if (input.equals(EAT))
							pointer++;
						else if (input.equals(PUKE))
							pointer--;
						else if (input.equals(POOP))
							temp += alphabet.charAt(pointer);
						else if (input.equals(POOPU))
							temp += Character.toUpperCase(alphabet.charAt(pointer));
						else if (input.equals(SNIFF))
							output.append(temp);
						else if (input.equals(FLUSH)) {
							pointer = 0;
							temp = "";
						}
					}
				}
			}
		} else {
			String[] split = poop.split(" ");
			for (String input : split) {
				if (!input.isEmpty()) {
					if (input.equals(EAT))
						pointer++;
					else if (input.equals(PUKE))
						pointer--;
					else if (input.equals(POOP))
						temp += alphabet.charAt(pointer);
					else if (input.equals(POOPU))
						temp += Character.toUpperCase(alphabet.charAt(pointer));
					else if (input.equals(SNIFF))
						output.append(temp + " ");
					else if (input.equals(FLUSH)) {
						pointer = 0;
						temp = "";
					}
				}
			}
		}
		return output.toString();
	}
	
	public static String toPoop(String line) {
		String output = "";
		int pointer = 0;
		char[] chars = line.toCharArray();
		for (char c : chars) {
			boolean uppercase = Character.isUpperCase(c);
			c = Character.toLowerCase(c);
			int index = alphabet.indexOf(c);
			if (pointer < index) {
				for (; pointer < index; pointer++)
					output += EAT + " ";
				output = output.substring(0, output.length() - 1);
			}
			else if (pointer > index) {
				for (; pointer > index; pointer--)
					output += PUKE + " ";
				output = output.substring(0, output.length() - 1);
			}
			output += "\n\n";
			if (uppercase)
				output += POOPU;
			else
				output += POOP;
			output += "\n\n";
		}
		output += SNIFF + "\n\n";
		output += FLUSH;
		return output;
	}
	
}
